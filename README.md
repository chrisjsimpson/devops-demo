# DevOps Demo

Demonstration of DevOps with a minimal viable example, to use as a reference to guide your own DevOps practice.

Very few assumptions are made about your application, this is on purpose so that teams can see how the pipelines, code, and DevOps processs demonstrated here can easily be applied to your own applications.


# Goals (see epic [#1](https://gitlab.com/chrisjsimpson/devops-demo/-/issues/1))
- React application with backend api
- Database demonstrating how to manage state and schema changes in DevOps pipelines
- Tests
- Security (SecOps)
- Pull Request / Review Process
  - Demonstration of migrating a stateful application (e.g. database migrations, schema change)
- Access control
- PR Preview environments
- Separation of environments (e.g. preview, qa, audit, performance, production)
- Infrastructure as code
- Backup Restore disaster recovery

# Getting started / Local development

There are two main application components

1. frontend - a react application
2. api
3. database - persistence layer

## Local development quickstart

For linux/mac:

[Install podman](https://podman.io/getting-started/installation)
Then build the frontend &amp; api container:

```
./build-api.sh
./build-frontend.sh
```

### Run the application

Start the api
```
./start-api.sh
```

In another terminal, start the frontend
```
./start-frontend.sh
```


TODO: [As a developer the quickstart guide is compatible with mac, linux, and windows users](https://gitlab.com/chrisjsimpson/devops-demo/-/issues/3)

### Running tests

Run the api tests:
```
./run-api-tests.sh
```


## Need help?

Collaborate with [FP Complete](https://www.fpcomplete.com/).

> [FP Complete](https://www.fpcomplete.com/) is your global full-stack technology partner that specializes in Server-Side Software, DevSecOps, Cloud Native Computing, and Advanced Programming Languages.
