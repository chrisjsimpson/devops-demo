#!/bin/bash

# Run tests
podman run fpcomplete/devopsdemo python -m pytest -vv
RETURN_CODE=$?

if [ $RETURN_CODE -ne 0 ]; then
  echo Tests failed. Did you rebuild the container?
  echo See detailed output above
else
  echo Tests passed.
fi
